Source: predict
Section: hamradio
Priority: optional
Maintainer: Debian Hamradio Maintainers <debian-hams@lists.debian.org>
Uploaders: Bdale Garbee <bdale@gag.com>,
           A. Maitland Bottoms <bottoms@debian.org>
Build-Depends: cmake (>=3.7.2),
               debhelper (>= 12),
               libgtk2.0-dev,
               libncurses5-dev
Standards-Version: 4.1.5
Homepage: http://www.qsl.net/kd2bd/predict.html
Vcs-Browser: https://salsa.debian.org/debian-hamradio-team/predict
Vcs-Git: https://salsa.debian.org/debian-hamradio-team/predict.git

Package: predict
Architecture: any
Depends: netbase (>= 4.07), ${misc:Depends}, ${shlibs:Depends}
Suggests: ntp, predict-gsat
Description: Satellite Tracking Program with Optional Voice Output
 This is a satellite tracking program.  It is probably mostly of interest to
 users of amateur satellites, but includes support for optionally announcing
 azimuth and elevation to help in manual antenna pointing, or optical
 observation of satellites.
 .
 The upstream predict sources include a front-end called 'map', which is
 called predict-map in the Debian package.
 .
 The 'ntp' package is suggested because accurate satellite tracking depends
 on precise knowledge of ground station time and location.

Package: predict-gsat
Architecture: any
Depends: netbase (>= 4.07), ${misc:Depends}, ${shlibs:Depends}
Suggests: ntp, predict
Description: Graphical Satellite Tracking Client Program
 The gsat program is a graphical client for the 'predict' satellite tracking
 program running in server mode, built using gtk.
 .
 Since this program can be run on a different machine than predict, there is
 no dependency specified... but you need access to a copy of 'predict'
 installed somewhere on the network for this programs to be useful!
